document.addEventListener("DOMContentLoaded",function (){
    let slides = document.querySelector("div.product-images img");//grandImage
    let minia = document.querySelector("div.product-miniatures")
    let imgs = minia.getElementsByTagName('div');//miniature


    for (let img of imgs){

        img.addEventListener('click',function (){
            //slides.setAttribute('src',img.src);
            slides.src=img.querySelector("img").src
        })
    }


    let plus = document.getElementById('plus');
    let moins= document.getElementById('moins');
    let number=document.getElementById('number');
    let text=document.getElementById('text');
    let maxNumber =5;

 text.style.visibility= "hidden";
    plus.addEventListener('click',function () {

        if(number.textContent<maxNumber){
            number.textContent= parseInt(number.textContent)+1;

            if (number.textContent>=maxNumber){
                text.style.visibility='visible';
            }

        }
    })

    moins.addEventListener('click',function(){
        if(number.textContent>0){
            number.textContent= parseInt(number.textContent)-1;

            if (number.textContent < maxNumber) {text.style.visibility='hidden';}


        }
    })


})