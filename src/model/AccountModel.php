<?php


namespace model;


class AccountModel
{
    static function check($firstname, $lastname, $mail, $password){
        if(strlen($firstname)<2 || strlen($lastname)<2 || !filter_var($mail,FILTER_VALIDATE_EMAIL) || strlen($password)<6){
            return false;
        }
        // Connexion à la base de données
        $db =\model\Model::connect();

        // Requête SQL
        $sql = "SELECT * FROM account where account.mail= '$mail'";

        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute();


        if(!count( $req->fetchAll())==0){
            return false;
        }
        return true;
    }


    static function signin($firstname, $lastname, $mail, $password){
        if(!self::check($firstname, $lastname, $mail, $password)){
           return false;
        }
        // Connexion à la base de données
        $db = \model\Model::connect();
        $pass = password_hash($password,PASSWORD_DEFAULT);
        // Requête SQL
        $sql = "INSERT INTO account (firstname, lastname, mail, password) VALUES(?, ?, ?, ?)";
        $req = $db->prepare($sql);
        $req->execute(array($firstname, $lastname, $mail, $pass));

        return true;

    }
    static function login($mail, $password):array
    {
        // Connexion à la base de données
        $db = \model\Model::connect();
        $pass = password_hash($password,PASSWORD_DEFAULT);
        // Requête SQL
        $sql = "SELECT *FROM account WHERE account.mail= '$mail'";
        $req = $db->prepare($sql);
        $req->execute();
        $resp = $req->fetchAll();

        if(password_verify($password,$resp[0]['password'])){
            return $resp;
        }else{
            return array();
        }

    }

    static function update($firstname,$lastname,$mail, $id): bool
    {
        $db = \model\Model::connect();

        $sql = "UPDATE account SET firstname='$firstname',lastname='$lastname',mail='$mail' WHERE account.id=$id";
        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute();
        return true;
    }



}