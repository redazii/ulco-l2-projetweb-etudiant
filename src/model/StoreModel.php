<?php

namespace model;

class StoreModel {

  static function listCategories(): array
  {
    // Connexion à la base de données
    $db = \model\Model::connect();

    // Requête SQL
    $sql = "SELECT id, name FROM category";
    
    // Exécution de la requête
    $req = $db->prepare($sql);
    $req->execute();

    // Retourner les résultats (type array)
    return $req->fetchAll();
  }
    static function listproducts():array
    {
        // Connexion à la base de données
        $db = \model\Model::connect();

        // Requête SQL
        $sql = "SELECT product.id, product.name,product.price,product.image, category.name as Name_category FROM product INNER JOIN  category ON product.category   = category.id";

        // Exécution de la requête
        $request = $db->prepare($sql);
        $request ->execute();

        // Retourner les résultats (type array)
        return $request ->fetchAll();

    }
    static function infoProduct(int $id)
    {
        // Connexion à la base de données
        $db = \model\Model::connect();

        //Requete Sql
        $sql= "SELECT p.name,p.id, p.price, p.image,p.image_alt1,p.image_alt2,p.image_alt3, p.spec,c.name AS nameCategory FROM product AS p INNER JOIN category as c ON (p.category = c.id) WHERE p.id = $id";
        //Exécution de la requete
        $request = $db->prepare($sql);
       $request->execute();
       // $request->execute(['id'=>$id]);
         //$request ->execute(array($id));

        //Retourne les resultat dans un array
        return $request->fetch();

    }

    static function searchProduct($search, $category, $order){
        $db = \model\Model::connect();
        $sql = "SELECT p.id, p.name, p.price, p.image, c.name AS Name_category FROM product  AS p INNER JOIN category as c WHERE p.category = c.id ";
        if ($category!=""){
            $sql.=" AND (";
            foreach ($category as $c) {
                if ($c==$category[0]) {
                    $sql.=' c.name LIKE "' . $c . '"';
                } else {
                    $sql.=' OR c.name LIKE "' . $c . '"';
                }
            }
            $sql.=") ";
        }

        if ($search!=""){
            $sql.=' AND p.name LIKE ?';
        }

        if($order!="") {
            if ($order=="Croissant") {
                $sql.="  ORDER BY p.price ASC";
            }
            if ($order=="Décroissant") {
                $sql.="  ORDER BY p.price DESC";
            }
        }

        $req = $db->prepare($sql);
        if ($search!="") {
            $req->execute(array("%" . $search . "%"));
        } else {
            $req->execute();
        }

        return $req->fetchAll();
    }
}