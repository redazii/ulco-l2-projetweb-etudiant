<?php


namespace controller;


class CommentController
{
    public function postComment(int $id_product){

        $content=trim(htmlspecialchars($_POST['content']));

        if (!empty($_SESSION['logger'])){
            $id_account=$_SESSION['logger']['id'];
        }
        // var_dump($content,$id_product,$id_account);
      // \model\CommentModel::insertComment($content,$id_product,$id_account);
        \model\CommentModel::insertComment( $id_product,  $id_account,  $content);

       header("Location: /store/$id_product");
    //  exit();


    }


}