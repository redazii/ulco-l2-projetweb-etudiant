<?php


namespace controller;


class AccountController
{
    public function account(){
        $params = [
            "title"=>"Account",
            "module"=>"account.php"
        ];

        // Faire le rendu de la vue "src/view/Template.php"
        \view\Template::render($params);
    }

    public function signin(){
        $userFirst = htmlspecialchars($_POST['userfirstname']);
        $userLast = htmlspecialchars($_POST['userlastname']);
        $userEmail = htmlspecialchars($_POST['usermail']);
        $userPass = htmlspecialchars($_POST['userpass']);

       if(\model\AccountModel::signin($userFirst,$userLast,$userEmail,$userPass)){
           header("Location: /account?status=signin_success");
       }else{
           header("Location: /account?status=signin_fail");
       }

    }

    public function login():void{
        $userMail= htmlspecialchars($_POST['usermail']);
        $userPass= htmlspecialchars($_POST['userpass']);

        $logger=\model\AccountModel::login($userMail,$userPass);

        if(count($logger)>0){
            $session=[
                "id"=>$logger[0]['id'],
                "firstname"=>$logger[0]['firstname'],
                "lastname"=>$logger[0]['lastname'],
                "mail"=>$logger[0]['mail']
            ];
            $_SESSION['logger']=$session;
            header("Location: /store");
        }else{
            header("Location: /account?status=login_fail");
        }

    }

    public function logout(){
        session_destroy();
        header("Location: /account?status=logout_success");
    }
    public function infos(){

        $params = [
            "title"=>"infos",
            "module"=>"infos.php"
        ];

        // Faire le rendu de la vue "src/view/Template.php"
        \view\Template::render($params);


    }

    public function update(){
        $firstname=htmlspecialchars($_POST['firstname']);
        $lastname=htmlspecialchars($_POST['lastname']);
        $mail=htmlspecialchars($_POST['mail']);
        $id=$_SESSION['logger']['id'];

         if (\model\AccountModel::update($firstname,$lastname,$mail,$id)) {
             $_SESSION['logger']['firstname']=$firstname;
             $_SESSION['logger']['lastname']=$lastname;
             $_SESSION['logger']['mail']=$mail;

             header("Location: /account/infos?status=update_success");
             exit();
         }
    }

}