<?php

namespace controller;

class StoreController {

  public function store(): void
  {

    $categories = \model\StoreModel::listCategories();
    $listproducts =\model\StoreModel::listproducts();


    $params = array(
      "title" => "Store",
      "module" => "store.php",
      "categories" => $categories,
        "listProducts"=>$listproducts
    );

    // Faire le rendu de la vue "src/view/Template.php"
    \view\Template::render($params);
  }

  public function product(int $id): void
  {
      $infoProduct=\model\StoreModel::infoProduct($id);
        $comment=\model\CommentModel::listComment($id);
      if($infoProduct!=null)
      {
          $params= array(
              "title"=>"produit",
              "module"=>"product.php",
              "infoProduct"=>$infoProduct,
              "comment"=>$comment
          );

          // Faire le rendu de la vue "src/view/Template.php"
          \view\Template::render($params);
    }else{
          header("Location: /store");
          exit();
      }

  }

  public function search(){

      $search = "";
      $order = "";
      $category ="";
      if (isset($_POST['search']) and isset( $_POST['category']) ){
          $search=$_POST['search'];
          $category=$_POST['category'];
      }

      if (!empty($_POST['order'])) {
          $order=$_POST['order'];
      }
      $categories = \model\StoreModel::listCategories();
      $listProducts = \model\StoreModel::searchProduct($search, $category, $order);

      $params = array(
          "title" => "Store",
          "module" => "store.php",
          "categories" => $categories,
          "listProducts" => $listProducts
      );

      \view\Template::render($params);

  }

}