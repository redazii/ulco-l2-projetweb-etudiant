<div id="product">
    <div>
        <div class="product-images">
            <img src="/public/images/<?= $params['infoProduct']['image']?>" alt="">
            
            <div class="product-miniatures">
                <div>
                    <img src="/public/images/<?= $params['infoProduct']['image']?>" alt="">
                </div>

                <div>
                    <img src="/public/images/<?= $params['infoProduct']['image_alt1']?>" alt="">
                </div>

                <div>
                    <img src="/public/images/<?= $params['infoProduct']['image_alt2']?>" alt="">
                </div>
                <div>
                    <img src="/public/images/<?= $params['infoProduct']['image_alt3']?>" alt="">
                </div>


            </div>


        </div>
        <div class="product-infos">
            <p class="product-category"><?=$params['infoProduct']['nameCategory'] ?></p>
            <h1><?= $params['infoProduct']['name']?></h1>

            <p class="product-price">
                <?= $params['infoProduct']['price']?>
            </p>
            <form >
                <button id="moins"  type="button">-</button>
                <button id="number" type="button">1</button>
                <button id="plus" type="button">+</button>
                <input type="submit" value="Ajouter dans panier">
            </form>
            <div id="text" class="box error" >
                Quantité maximale autorisée !
            </div>
        </div>
        
    </div>
    <div>
        <div class="product-spec">
            <h2>Spécificités</h2>
            <?= $params['infoProduct']['spec']?>
        </div>
        <div class="product-comments">
            <h2>Avis</h2>
            <?php if($params['comment']!=null) {
                foreach ($params['comment'] as $c){
                ?>

            <div class="product-comment">
                <p class="product-comment-author"><?= $c['firstname']." ".$c['lastname'] ?></p>
                <p>
                   <?= $c['content'] ?>
                </p>
            </div>
            <?php  }
            } else{ ?>
            <p>Il n'y a pas d'avis pour ce produit</p>
            <?php } if (!empty($_SESSION['logger'])) {?>

            <form method="post" action="/store/postComment/<?=$params['infoProduct']['id'] ?>">
                <input type="text" name="content" placeholder="Rédiger un commentaire">
            </form>
            <?php }?>
        </div>
    </div>
</div>

<script src="/public/scripts/product.js"></script>